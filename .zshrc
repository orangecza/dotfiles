# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/cza/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# {{{

# Powerlevel9k Theme Settings{{{

ZSH_THEME="powerlevel10k/powerlevel10k"

POWERLEVEL9K_MODE="nerdfont-complete"

POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_RPROMPT_ON_NEWLINE=true
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true

POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR=$'\uE0B4'
POWERLEVEL9K_RIGHT_SEGMENT_SEPARATOR=$'\uE0B2'

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(host dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status user time)

POWERLEVEL9K_VCS_BRANCH_ICON=$'\uF126 '

POWERLEVEL9K_TIME_BACKGROUND='197'
POWERLEVEL9K_TIME_ICON=$'\uf017'

POWERLEVEL9K_USER_DEFAULT_BACKGROUND='214'
POWERLEVEL9K_USER_DEFAULT_FOREGROUND='054'
POWERLEVEL9K_USER_ICON="\uF415"

POWERLEVEL9K_HOST_LOCAL_BACKGROUND='023'
POWERLEVEL9K_HOST_LOCAL_FOREGROUND='015'
POWERLEVEL9K_HOST_ICON="\uF109 "
POWERLEVEL9K_SSH_ICON="\uF489 "

POWERLEVEL9K_VCS_CLEAN_FOREGROUND='blue'
POWERLEVEL9K_VCS_CLEAN_BACKGROUND='black'

POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND='yellow'
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='black'

POWERLEVEL9K_VCS_MODIFIED_FOREGROUND='125'
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='black'

POWERLEVEL9K_VI_MODE_INSERT_FOREGROUND='204'

POWERLEVEL9K_DIR_NOT_WRITABLE_FOREGROUND='196'

POWERLEVEL9K_HOME_ICON=$'\uf015 '
POWERLEVEL9K_FOLDER_ICON=$'\uf07b '
POWERLEVEL9K_HOME_SUB_ICON=$'\uf07c '

POWERLEVEL9K_DIR_PATH_SEPARATOR="%F{black} $(print $'\ue0b5') %F{black}"
# }}}

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Zsh Settings{{{

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="false"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder
# }}}

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-syntax-highlighting z vi-mode incr)
#ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=cyan"
#ZSH_AUTOSUGGEST_STRATEGY=(completion match_prev_cmd)
#ZSH_AUTOSUGGEST_USE_ASYNC=1

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Don't compelete paste
pasteinit() {
  OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
  zle -N self-insert url-quote-magic # I wonder if you'd need `.url-quote-magic`?
}

pastefinish() {
  zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish

# Compeletion Boost up
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#}}}

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# {{{
alias logout="pkill i3"
alias v="vim"
alias r="ranger"
alias n="neofetch"
# alias rm -rf="trash"
alias rm="trash"
alias truerm="/bin/rm"
alias proxy="export all_proxy=socks5://localhost:1080"
alias PROXY_PREFIX="https_proxy=http://127.0.0.1:12333 http_proxy=http://127.0.0.1:12333"
alias suspend="sudo systemctl suspend"
alias cdlog="cd /var/log"
alias q="exit"
alias zshrc="vim ~/.zshrc"
alias vimrc="vim ~/.config/nvim/.vim/vimrc"
alias sl="ls"
alias i3cfg="vim ~/.config/i3/config"
alias keyup="xset r rate 140 40"
alias pdf="zathura"
alias video="mpv"
alias python="python3.8"
alias synctime="sudo ntpdate ntp.ntsc.ac.cn"
alias resetsound="alsactl restore"
alias lsusr="cat /etc/passwd"
alias memfree="sync && sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'"
alias screenoff="xset dpms force off"
alias rmorphans="sudo pacman -Rns $(pacman -Qtdq)"
alias rmoptpkgs="sudo pacman -Rns $(pacman -Qttdq)"
alias picfg="vim ~/.config/picom/picom.conf"
alias so="source ~/.zshrc"
alias gen_compl_db="cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1"
alias track_dotfile="git --git-dir=$HOME/.config/.files.git --work-tree=$HOME"
alias wifion="nmcli radio wifi on"
alias wifioff="nmcli radio wifi off"
alias doom="PROXY_PREFIX ~/.emacs.d/bin/doom"
alias e="emacsclient"
alias er="systemctl --user restart emacs"
alias ssr='sh -c "nohup /opt/appimg/ssr >> /dev/null 2>&1 &"'
# alias git_key="ssh-agent zsh && ssh-add ~/.ssh/git" #The second command won't effect
# }}}

export JAVA_HOME="/usr/lib/jvm/java-11-openjdk"
export JAVA_BIN="$JAVA_HOME/bin"
export XDG_CONFIG_HOME="$HOME/.config"
export PATH="$PATH:$JAVA_BIN:/opt/appimg"

# Vi-mode Settings# {{{
typeset -A key

key[Home]=${terminfo[khome]}

key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

# setup key accordingly
[[ -n "${key[Home]}"     ]]  && bindkey  "${key[Home]}"     beginning-of-line
[[ -n "${key[End]}"      ]]  && bindkey  "${key[End]}"      end-of-line
[[ -n "${key[Insert]}"   ]]  && bindkey  "${key[Insert]}"   overwrite-mode
[[ -n "${key[Delete]}"   ]]  && bindkey  "${key[Delete]}"   delete-char
[[ -n "${key[Up]}"       ]]  && bindkey  "${key[Up]}"       up-line-or-history
[[ -n "${key[Down]}"     ]]  && bindkey  "${key[Down]}"     down-line-or-history
[[ -n "${key[Left]}"     ]]  && bindkey  "${key[Left]}"     backward-char
[[ -n "${key[Right]}"    ]]  && bindkey  "${key[Right]}"    forward-char
[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"   beginning-of-buffer-or-history
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}" end-of-buffer-or-history

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init () {
        printf '%s' "${terminfo[smkx]}"
    }
    function zle-line-finish () {
        printf '%s' "${terminfo[rmkx]}"
    }
    zle -N zle-line-init
    zle -N zle-line-finish
fi

zle-keymap-select () {
   if [[ ${KEYMAP} == vicmd ]]; then
      # the command mode for vi
      echo -ne "\e[2 q"
   else
      # the insert mode for vi
      echo -ne "\e[5 q"
   fi
}

KEYTIMEOUT=5

precmd_functions+=(zle-keymap-select)
# }}}

# Compeletion Key bind
#bindkey "" autosuggest-accept
#bindkey "" autosuggest-execute

# fzf key bind
#export FZF_COMPLETION_TRIGGER=''
#bindkey '^T' fzf-completion
#bindkey '^I' $fzf_default_completion

export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

export GTK_IM_MODULE=ibus
export QT_IM_MODULE=ibus
export XMODIFIERS=@im=ibus

cat ~/TODO
