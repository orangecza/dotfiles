#!/bin/zsh

git --git-dir=$HOME/.config/.files.git --work-tree=$HOME add \
~/.config/gtk-3.0/settings.ini \
~/.config/bashtop/bashtop.cfg \
~/.config/i3/ \
~/.config/mimeapps.list \
~/.config/networkmanager-dmenu/config.ini \
~/.config/nvim/.vim/autoload/custom_func/ \
~/.config/nvim/.vim/autoload/plug.vim \
~/.config/nvim/.vim/vimrc \
~/.config/nvim/init.vim \
~/.config/picom.conf \
~/.config/polybar/config.ini \
~/.config/polybar/launch.sh \
~/.config/ranger/colorschemes/__init__.py \
~/.config/ranger/colorschemes/euph.py \
~/.config/ranger/colorschemes/r.py \
~/.config/ranger/commands.py \
~/.config/ranger/commands_full.py \
~/.config/ranger/plugins/__init__.py \
~/.config/ranger/plugins/devicons.py \
~/.config/ranger/plugins/devicons_linemode.py \
~/.config/ranger/rc.conf \
~/.config/ranger/rifle.conf \
~/.config/ranger/scope.sh \
~/.config/yay/config.json \
~/.config/zathura/zathurarc \
~/.config/neofetch/config.conf \
~/.config/.files.git/track.sh \
~/.xinitrc \
~/.zshrc \

git --git-dir=$HOME/.config/.files.git --work-tree=$HOME \
commit -m "Update"

git --git-dir=$HOME/.config/.files.git --work-tree=$HOME \
push -u dot --all
