#!/bin/zsh

if [ -z `cat /proc/acpi/ibm/fan | grep  "level:		disengaged"` ];then
   echo level full-speed | sudo tee /proc/acpi/ibm/fan 2>&1 >/dev/null
   echo 0 blink | sudo tee /proc/acpi/ibm/led 2>&1 >/dev/null 
   exit;
else
   echo level auto | sudo tee /proc/acpi/ibm/fan 2>&1 >/dev/null
   echo 0 on | sudo tee /proc/acpi/ibm/led 2>&1 >/dev/null 
fi
