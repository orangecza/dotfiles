#!/bin/zsh

let brightness=$((`cat /sys/class/backlight/amdgpu_bl0/brightness`-15));

if [ $brightness -le 0 ];then
   echo 0 >/sys/class/backlight/amdgpu_bl0/brightness;
   exit;
fi

echo $brightness > /sys/class/backlight/amdgpu_bl0/brightness;
