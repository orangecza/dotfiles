#!/bin/zsh

let brightness=$((`cat /sys/class/backlight/amdgpu_bl0/brightness`+15));

if [ $brightness -ge 255 ];then
   echo 255 >/sys/class/backlight/amdgpu_bl0/brightness;
   exit;
fi

echo $brightness > /sys/class/backlight/amdgpu_bl0/brightness;
