"VIM配置---------------------------------------------------
set nu"{{{
syntax on
let g:mapleader=";"
set ruler
set mouse-=a
set nocompatible
set scrolloff=10
set updatetime=100
set hidden
set termguicolors
set signcolumn=yes
set shortmess+=c

"缩进设置
set cindent

"搜索忽略大小写
set ignorecase
set smartcase

"光标所在行高亮
set cursorline

"VIM小插件路径
"set packpath=plugin
"使用 vim-plug 插件管理

"自动重加载
set autoread

"设置折叠
set fdm=syntax

"设置TAB和缩进宽度
set noet
set tabstop=8
set shiftwidth=8
set autoindent

"80字符线
set cc=80

"修改后自动加载vimrc
autocmd! bufwritepost .vimrc source %
autocmd! bufwritepost vimrc source %

"命令补全与提示
set wildmenu
set wildmode=full

"设置默认剪切板
set clipboard=unnamedplus
let g:clipboard = {
	  \   'name': 'SystemClipboard',
	  \   'copy': {
	  \      '+': 'xclip -i -selection clipboard',
	  \      '*': 'xclip -i -selection clipboard',
	  \    },
	  \   'paste': {
	  \      '+': 'xclip -o -selection clipboard',
	  \      '*': 'xclip -o -selection clipboard',
	  \   },
	  \   'cache_enabled': 1,
	  \ }

"设置退格功能
set backspace=2

"设置编码
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set fileencoding=utf-8
set termencoding=utf-8
set encoding=utf-8

"光标形状
let &t_SI.="\e[5 q" "插入模式光标
let &t_SR.="\e[4 q" "替换模式光标
let &t_EI.="\e[1 q" "正常模式光标

" 缩进提示
set listchars=tab:┊\ ǁ,trail:␣,nbsp:+,extends:↹ ",eol:↵
set list

"let guicursor

"}}}

"安装的插件------------------------------------------------
"{{{
execute "call plug#begin('" . expand('<sfile>:p:h') . "/my_plugins/')" 

"快速补全HTML
"Plug 'mattn/emmet-vim'

"代码片段
Plug 'honza/vim-snippets'
Plug 'SirVer/ultisnips'

"NERD注释
Plug 'preservim/nerdcommenter'

"主题
"Plug 'altercation/vim-colors-solarized'
"Plug 'morhetz/gruvbox'
"Plug 'connorholyday/vim-snazzy'
"Plug 'dracula/vim', { 'as': 'dracula' }
"Plug 'gruvbox-material/vim', {'as': 'gruvbox-material'}
Plug 'sainnhe/sonokai'
"
"Plug 'cocopon/iceberg.vim'
"Plug 'ryanoasis/vim-devicons'

"输入法自动切换
"Plug 'kevinhwang91/vim-ibus-sw'

"文件管理
"Plug 'preservim/nerdtree' "| Plug 'Xuyuanp/nerdtree-git-plugin'
"Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
"Plug 'kevinhwang91/rnvimr'
Plug 'francoiscabrol/ranger.vim'
Plug 'rbgrouleff/bclose.vim'

"自由编写
Plug 'junegunn/goyo.vim'
Plug 'amix/vim-zenroom2'

"美化状态栏
Plug 'vim-airline/vim-airline'

"大项目模糊查找
Plug 'ctrlpvim/ctrlp.vim'

"括号补全
"Plug 'Raimondi/delimitMate'
Plug 'jiangmiao/auto-pairs'

"显示缩进
"Plug 'Yggdroot/indentLine'

"单词下划线
Plug 'itchyny/vim-cursorword'

"单词变色
"Plug 'osyo-manga/vim-brightest'

"快速符号包围
Plug 'tpope/vim-surround'

"语法检查和自动补全
"Plug 'dense-analysis/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'ycm-core/YouCompleteMe', {'do':'./install.py --all --clangd-completer'}
"Plug 'vim-syntastic/syntastic'

"代码大纲
"Plug 'majutsushi/tagbar'

"MarkDown
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }

"更好的括号匹配
Plug 'andymass/vim-matchup'

"nvim 提权
Plug 'lambdalisue/suda.vim'

"Git diff标记
Plug 'airblade/vim-gitgutter'

"语言包
"Plug 'sheerun/vim-polyglot'
Plug 'jackguo380/vim-lsp-cxx-highlight'

"轻松对齐
Plug 'junegunn/vim-easy-align'

"Rime输入法
"Plug 'infmagic2047/vim-rime-im'
"Plug 'h-youhei/vim-ibus'
Plug 'rlue/vim-barbaric'

call plug#end()
"}}}

"插件配置--------------------------------------------------
"{{{

"停用libpinyin
"source autoload/ibus.vim

"多光标
"nmap <silent> <C-c> <Plug>(coc-cursors-position) nmap <silent> <C-d> <Plug>(coc-cursors-word) xmap <silent> <C-d> <Plug>(coc-cursors-range)
" use normal command like `<leader>xi(`
"nmap <leader>x  <Plug>(coc-cursors-operator)


"TermDebug设置 
let g:termdebug_wide = 10

"括号补全设置
let g:AutoPairsFlyMode = 0
let g:AutoPairsMultilineClose = 0

"主题设置
set termguicolors
set background=dark
let g:sonokai_style = 'andromeda'
let g:sonokai_enable_italic = 1
let g:sonokai_disable_italic_comment = 1
"let g:sonokai_transparent_background = 1

colorscheme sonokai

"GitGutter设置
let g:gitgutter_sign_allow_clobber = 1
highlight SignColumn guibg=whatever

"Airline设置
let g:airline_theme = 'sonokai'
let g:airline_extensions = ['branch', 'tabline', 'coc', 'hunks']
let g:airline_left_sep=''
let g:airline_right_sep = ''
let g:airline_powerline_fonts = 1
let g:airline_detect_modified=1
let g:airline_detect_spell=1
let g:airline#extensions#tabline#show_splits = 1
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#show_tabs = 1
let g:airline#extensions#tabline#show_tab_count = 2
let g:airline#extensions#tabline#switch_buffers_and_tabs = 1
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#coc#enabled = 1
let g:airline#extensions#coc#error_symbol = 'E:'
let g:airline#extensions#coc#warning_symbol = 'W:'
let g:airline_highlighting_cache = 1 " 提高性能

"自动补全设置
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
"inoremap <silent><expr> <C-space> coc#refresh()
"autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif
"let g:coc_snippet_next = '<tab>'
"imap <C-l> <Plug>(coc-snippets-expand)
"map <leader>rn <Plug>(coc-rename)

"挂起代码片段
let g:UltiSnipsExpandTrigger="<C-l>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<S-tab>"

"语法检查和自动补全 
" ALE 
"let g:ale_linters = {
      "\'c++': ['ccls'],
      "\'c': ['ccls'],
      "\'python': ['pyls'],
    "\}
"let g:ale_c_ccls_init_options = {
    "\   'cacheDirectory': '/tmp/ccls',
    "\   'cacheFormat': 'binary',
    "\   'diagnostics': {
    "\     'onOpen': 0,
    "\     'opChange': 300,
    "\   },
    "\}
"let g:ale_set_balloons = 1
"let g:ale_lint_on_insert_leave = 1
"let g:ale_sign_column_always = 1
"let g:ale_set_highlights = 1
"let g:ale_sign_error = 'E'
"let g:ale_sign_warning = 'W'
"let g:ale_completion_enabled = 1
"let g:ale_completion_max_suggestions = 10
"let g:airline#extensions#ale#enabled = 1
""{{{ ale_completion_symbols
"let g:ale_completion_symbols = {  
	  "\ 'text': '',
	  "\ 'method': '',
	  "\ 'function': '',
	  "\ 'constructor': '',
	  "\ 'field': '',
	  "\ 'variable': '',
	  "\ 'class': '',
	  "\ 'interface': '',
	  "\ 'module': '',
	  "\ 'property': '',
	  "\ 'unit': 'unit',
	  "\ 'value': 'val',
	  "\ 'enum': '',
	  "\ 'keyword': 'keyword',
	  "\ 'snippet': '',
	  "\ 'color': 'color',
	  "\ 'file': '',
	  "\ 'reference': 'ref',
	  "\ 'folder': '',
	  "\ 'enum member': '',
	  "\ 'constant': '',
	  "\ 'struct': '',
	  "\ 'event': 'event',
	  "\ 'operator': '',
	  "\ 'type_parameter': 'type param',
	  "\ '<default>': 'v'
  "\ } 
""}}}
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%* 
" }}}
" Syntastic {{{
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0
"}}}
" {{{ coc.nvim
nnoremap <silent> <leader>L <Plug>(coc-diagnostic-prev)
nnoremap <silent> <leader>l <Plug>(coc-diagnostic-next)
nnoremap <silent> gd <Plug>(coc-definition)
nnoremap <silent> gy <Plug>(coc-type-definition)
nnoremap <silent> gi <Plug>(coc-implementation)
nnoremap <silent> gr <Plug>(coc-references)
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction
" }}}

"括号匹配
let g:matchup_matchparen_deferred = 1
let g:matchup_matchparen_timeout = 70

"MarkDown
let g:mkdp_auto_start = 0
"let g:mkdp_open_to_the_world = 0
"let g:mkdp_browser = 'firefox'
let g:mkdp_browserfunc = 'custom_func#mkdp_firefox#new_window'
let g:mkdp_open_ip = '127.0.0.1'

"自动提权
let g:suda_smart_edit = 1

"轻松对齐
let g:easy_align_ignore_groups = ['String']

"单词下划线
let g:cursorword_delay = 0
let g:cursorword_highlight = 0

" 输入法
"let g:ibus#engine = 'rime'

" ranger
"let g:NERDTreeDirArrowExpandable = '+'
"let g:NERDTreeDirArrowCollapsible = '-'
"let g:NERDTreeGitStatusUpdateOnCursorHold = 1
let g:ranger_replace_netrw = 1

"}}}

"按键映射--------------------------------------------------
"{{{
"杂项
noremap s <nop> "取消s的功能

"分屏
nnoremap <Leader>vs :vs<CR>

"重复f-搜索
nnoremap \ ;

"退出,保存
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>

"取消高亮,刷新屏幕,读配置
nnoremap <Leader><CR> :noh<CR>:redraw<CR>:so ~/.config/nvim/.vim/vimrc<CR>

"语法检查映射
"ALE{{{
"nnoremap <C-n> :ALENextWrap<CR>
"nnoremap <C-m> :ALEPreviousWrap<CR>
"nnoremap <Leader>d :ALEDetail<CR>
"nnoremap <silent> gd :ALEGoToDefinition<CR>
"nnoremap <silent> gr :ALEFindReferences<CR>
"}}}
"COC
nmap <silent> <leader>L <Plug>(coc-diagnostic-prev)
nmap <silent> <leader>l <Plug>(coc-diagnostic-next)
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Coc重命名
nmap <leader>rn <Plug>(coc-rename)

"挂起代码大纲
nnoremap <F8> :TagbarToggle<CR>

"窗口切换
nmap <C-j> <C-W>j
nmap <C-k> <C-W>k
nmap <C-h> <C-W>h
nmap <C-l> <C-W>l

"窗口位置切换
verbose nmap <leader>hh <C-W>H
verbose nmap <leader>jj <C-W>J
verbose nmap <leader>kk <C-W>K
verbose nmap <leader>ll <C-W>L

"标签管理
map <leader>tn :tabnew<cr>
map <leader>th gT
map <leader>tl gt

"选中代码格式化
xmap <leader>=  <Plug>(coc-format-selected)
nmap <leader>=  <Plug>(coc-format-selected)

"Markdown
vnoremap <leader>b <esc>`>a**<esc>`<i**<esc>
vnoremap <leader>bi <esc>`>a***<esc>`<i***<esc>
vnoremap <leader>i <esc>`>a*<esc>`<i*<esc>

"可视化查找
vmap / y/<C-R>"<CR>
vmap ? y/<C-R>"<CR>
xnoremap * :<C-u>call <SID>VSetSearch('/')<CR>/<C-R>=@/<CR><CR> 
xnoremap # :<C-u>call <SID>VSetSearch('?')<CR>?<C-R>=@/<CR><CR>

function! s:VSetSearch(cmdtype)
      ""let temp = @s
      ""norm! gv"sy
      ""let @/ = '\V' . substitute(escape(@s, a:cmdtype.'\'), '\n', '\\n', 'g') 
      ""let @s = temp
endfunction

"NERDTree
"nnoremap <leader>nn :NERDTreeToggle<cr>
"nnoremap <leader>nb :NERDTreeFromBookmark
"nnoremap <leader>nf :NERDTreeFind<cr>
nnoremap <leader>nn :vs<cr>:ranger<cr>

"Zenroom
nnoremap <silent> <leader>z :Goyo<cr>

"MarkDown
nnoremap <m-s> <Plug>MarkdownPreviewStop
nnoremap <m-p> <Plug>MarkdownPreview
nnoremap <m-t> <Plug>MarkdownPreviewToggle

"ESC映射
"inoremap jj <esc>
"cnoremap jj <esc>
"使用系统映射Caps为Esc

"终端快速跳转
tnoremap <C-h> <C-\><C-N><C-w>h
tnoremap <C-j> <C-\><C-N><C-w>j
tnoremap <C-k> <C-\><C-N><C-w>k
tnoremap <C-l> <C-\><C-N><C-w>l
tnoremap <leader>q <C-\><C-N>

"终端结束
tnoremap <C-;> <C-\><C-N>:
tnoremap :q<CR> exit<CR>

"调整大小
nnoremap <silent> <leader>= :vertical resize +5<CR>
nnoremap <silent> <leader>- :vertical resize -5<CR>

"轻松对齐
xnoremap <leader>a <Plug>(EasyAlign)
nnoremap <leader>a <Plug>(EasyAlign)
   "对齐大括号内注释(Cpp)
nnoremap <silent><leader>c= <ESC>:silent /}/,?{?EasyAlign ////<CR>:silent /}/,?{?EasyAlign //\*/<CR>
   "对齐选中注释
vnoremap <silent><leader>aa <ESC>:silent '<,'>EasyAlign ////<CR>:silent '<,'>EasyAlign //\*/<CR>
   "对齐大括号内分号(Cpp)
nnoremap <silent><leader>=; <ESC>:silent /}/,?{?EasyAlign /;/<CR>
   "对齐全部注释(Cpp)
nnoremap <silent><leader>c=a <ESC>:%EasyAlign ////<CR>
   "交互模式
nnoremap <leader>ia <ESC>:EasyAlign<CR>

"}}}
