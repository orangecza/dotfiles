execute 'set runtimepath+=' . expand('<sfile>:p:h') . '/.vim'
execute 'source ' . expand('<sfile>:p:h') . '/.vim/vimrc'
let &packpath = &runtimepath

" For debug plugin

"source ~/.config/nvim/.vim/plugin/ibus.vim

